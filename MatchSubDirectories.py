##===============================================================
# Created on: 11/15/2017
# Created by: Muni Melpati
# creates folder structure similar to the source file
#=============================================================
import arcpy, os, csv, ast,sys, logging

##creates folder structure similar to the source file
def main(mxdMatchingPairs=None):
	#get the dictionary object
	dictKeyValP = ast.literal_eval(mxdMatchingPairs)
	        
	try:
                
    		for key in dictKeyValP.keys():    			
    			realPath = os.path.normpath(dictKeyValP[key])			
        		createChilddirectories(realPath)		
    		arcpy.SetParameterAsText(1, True)    
    		arcpy.SetParameterAsText(2, False)  		
    		
	except: 
        	arcpy.SetParameterAsText(1, False)    
        	arcpy.SetParameterAsText(2, True) 
		logging.error("Error validating folder structure: " + e[0])		
                raise
def createChilddirectories (normPath):
    dirPathList = os.path.split(normPath)     
    collDirs = []    
    destDrive = None
    if len(dirPathList) > 1: dirPathList = os.path.split(dirPathList[0])    
    
    #split the path 
    while (len(dirPathList[1].strip())) > 0: 	
	collDirs.append(dirPathList[1])
	dirPathList = os.path.split(dirPathList[0])	
	if len(dirPathList[1].strip()) < 1 : destDrive = dirPathList[0]

    #Add any missing folders along the path of the mxd document
    for eachEntry in reversed(collDirs):	
	destDrive = os.path.join(destDrive,eachEntry)	
	if not os.path.isdir(destDrive): os.makedirs(destDrive)


mxdMatchingPairs = arcpy.GetParameterAsText(0)  

main(mxdMatchingPairs)




	

