# -*- coding: utf-8 -*-
# ---------------------------------------------------------------------------
# copymxds1.py
# Created on: 2017-11-22 20:41:33.00000
#   (generated by ArcGIS/ModelBuilder)
# Usage: copymxds1 <csv_datasource_pairs> <Map_Version> <Matching_MXD_pairs> <Database_Name> 
# Description: 
# ---------------------------------------------------------------------------

# Import arcpy module
import arcpy
import sys
import os
import traceback
import logging
import SawsEmailSender

arcpy.env.overwriteOutput = True
# Local variables:
curDir = ""
                         
def main(csvDatasourcePairs=None, csvMatchingMXDs=None,mapVersion=None,DatabaseName=None):
   try:         
         curDir = os.path.dirname(os.path.abspath(__file__)) 
         logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p',filemode='w', filename=curDir +'\MigrateMxd.log', level=logging.INFO)         
         logging.info('*****************Started************')                 
         # Load required toolboxes        
         filename = curDir + '\MigrateMxdstbx.tbx'
         #import the toolbox
         arcpy.ImportToolbox(filename)        
         #run the model/script
         arcpy.MigrateMXDs(csvDatasourcePairs,mapVersion,csvMatchingMXDs,DatabaseName)
         logging.info('Finished')         
   except:       
       logging.error("Migrate Mxds tool Failed.")
       SawsEmailSender.sendEmail("gis@saws.org","gis@saws.org","email test","this is email test", '')
#executing as a script
if __name__ == '__main__': 
   if len(sys.argv) == 5:        
      #passed through command line parameters
      csvDatasourcePairs = sys.argv[1]
      csvMatchingMXDs=sys.argv[2]
      mapVersion=sys.argv[3]
      DatabaseName=sys.argv[4]   
   else: 
      # When ran through arcMap/ArcCatalog
      print("reached aurgment input")      
      csvDatasourcePairs = arcpy.GetParameterAsText(0)
      mapVersion = arcpy.GetParameterAsText(2)
      csvMatchingMXDs = arcpy.GetParameterAsText(1)
      DatabaseName = arcpy.GetParameterAsText(3)
      
   main(csvDatasourcePairs,csvMatchingMXDs,mapVersion,DatabaseName)


