# Import smtplib for the actual sending function
#Emails log files to the sender
import smtplib, logging, sys,os, traceback
import mimetypes
from email import encoders
from email.message import Message
from email.mime.audio import MIMEAudio
from email.mime.base import MIMEBase
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

def sendEmail(fromEmail,toEmail,subject,message, attachmentFullPath):

        s = None
        mail = None               

        try:
                
                msg = MIMEMultipart()
                msg["From"] = fromEmail
                msg["To"] = toEmail
                msg["Subject"] = subject
                msg.preamble = subject
                if os.path.isfile(attachmentFullPath):  
                        ctype, encoding = mimetypes.guess_type(attachmentFullPath)
                        if ctype is None or encoding is not None:
                                ctype = "application/octet-stream"

                        maintype, subtype = ctype.split("/", 1)
                        if maintype == "text":
                            fp = open(attachmentFullPath)
                            # Note: we should handle calculating the charset
                            attachment = MIMEText(fp.read(), _subtype=subtype)
                            fp.close()
                        elif maintype == "image":
                            fp = open(attachmentFullPath, "rb")
                            attachment = MIMEImage(fp.read(), _subtype=subtype)
                            fp.close()
                        elif maintype == "audio":
                            fp = open(attachmentFullPath, "rb")
                            attachment = MIMEAudio(fp.read(), _subtype=subtype)
                            fp.close()
                        else:
                            fp = open(attachmentFullPath, "rb")
                            attachment = MIMEBase(maintype, subtype)
                            attachment.set_payload(fp.read())
                            fp.close()
                            encoders.encode_base64(attachment)
                           
                        attachment.add_header("Content-Disposition", "attachment", filename=attachmentFullPath)
                        msg.attach(attachment)
                s = smtplib.SMTP('mailhost.saws.org',25)
                print("reached3")
                s.sendmail(fromEmail, toEmail, msg.as_string())
                s.quit()
        except smtplib.SMTPAuthenticationError as e:
                logging.error("Warning: %s was caught while trying to send your mail.\nContent:%s\n" % (e.__class__.__name__,e.message))
        except smtplib.SMTPSenderRefused as e:
                logging.error("Warning: %s was caught while trying to send your mail.\nContent:%s\n" % (e.__class__.__name__,e.message))
        except smtplib.SMTPResponseException as e:
                logging.error("Warning: %s was caught while trying to send your mail.\nContent:%s\n" % (e.__class__.__name__,e.message))
        except smtplib.SMTPConnectError as e:
                tb = sys.exc_info()[2]
                tbinfo = traceback.format_tb(tb)[0]
                pymsg = "PYTHON ERRORS:\nTraceback Info:\n" + tbinfo + "\nError Info:\n    " + \
                                str(sys.exc_type)+ ": " + str(sys.exc_value) + "\n"
                logging.error("SendEmailError: " + pymsg)
                logging.error("Warning: %s was caught while trying to send your mail.\nContent:%s\n" % (e.__class__.__name__,e.message))
        except smtplib.SMTPException as e:
                logging.error("Warning: %s was caught while trying to send your mail.\nContent:%s\n" % (e.__class__.__name__,e.message))
        except smtplib.SMTPServerDisconnected as e:
                logging.error("Warning: %s was caught while trying to send your mail.\nContent:%s\n" % (e.__class__.__name__,e.message))
                
        finally:
                if s != None: s.quit()
