##===============================================================
# Created on: 11/15/2017
# Created by: Muni Melpati
# Copies mxds to a new folder and replaces datasources
#=============================================================
import arcpy, os, csv, logging, traceback
## Construct in memory key value pair
sourcecvsFile = arcpy.GetParameterAsText(0)
buildDict = {}
try:    
    with open(sourcecvsFile, 'rb') as outcsvfile:
        eachrow = csv.reader(outcsvfile, delimiter=',',quotechar='|')
        for row in eachrow:		
            if len(row) > 0:	      
        	buildDict [os.path.normpath(row[0])] = os.path.normpath(row[1])		
    arcpy.SetParameterAsText(1, buildDict)

except:    
        logging.error("Failed to build key/value pairs.")
        raise






