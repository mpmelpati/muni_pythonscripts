##===============================================================
# Created on: 11/15/2017
# Created by: Muni Melpati
# Copies mxds to a new folder and replaces datasource and dataset fullname
#=============================================================
import arcpy, os, csv, ast, sys, logging


def main():

	try:                
                #input parameters
		destDatabaseName =  arcpy.GetParameterAsText(0)
		mxdPairs = arcpy.GetParameterAsText(1)
		cvsFile = arcpy.GetParameterAsText(2)
		inputMapVersion = arcpy.GetParameterAsText(3)

		#get the dictionary object
		dictKeyValP = ast.literal_eval(cvsFile)
		dictMXDpairs = ast.literal_eval(mxdPairs)

		#iterates through mxd documents. for each data frame update layer and table views and 
		#save the mxd document to the destination File
		for sMXD in dictMXDpairs.keys():
			#for key in dictKeyValP.keys():		
			#	eachSMXD.findAndReplaceWorkspacePaths(os.path.realpath(key),os.path.realpath(dictKeyValP[key]),False)
		
			# iterate through each dataframe and update layer and table source info			
			UpdateMXDs(dictKeyValP,sMXD, dictMXDpairs,destDatabaseName)
			#raise Exception("test","test1")
	except:
		logging.error("Unexpected error while Copying mxds: " + str(sys.exc_info()[0]))			
		raise

def SetRelativePath (mxdObj, isSetRelative):
		mxdObj.relativePaths = isSetRelative


def UpdateMXDs(dictKeyValP,sMXD, dictMXDpairs, destDatabaseName):
	try:
		eachSMXD = arcpy.mapping.MapDocument(sMXD)	

		for df in arcpy.mapping.ListDataFrames(eachSMXD, ""):			
			ReplaceLayersSource(dictKeyValP,df,eachSMXD, destDatabaseName)
			ReplaceTableSource(dictKeyValP,df,eachSMXD,destDatabaseName)
		
		
		#set relative paths true
		SetRelativePath(eachSMXD,True)	
		eachSMXD.saveACopy(dictMXDpairs[sMXD])
		logging.info("saved " + dictMXDpairs[sMXD])
		del eachSMXD
	except:
		logging.error("Unexpected error whle updating mxds: " + str(sys.exc_info()[0]))	
		del eachSMXD
		raise
		

#Replaces layer datasources including layer full name (inputs: qualified datasource pairs, dataframe, mxd to be modified)
def ReplaceLayersSource(dictKeyValP, df, inputMXD, destDatabaseName):
	
	for lyr in arcpy.mapping.ListLayers(inputMXD, "",df):
		if not lyr.isGroupLayer:
			datasourceName = lyr.dataSource
			for key in dictKeyValP.keys():			
				cnt = datasourceName.find(os.path.realpath(key))
				if cnt > -1:
					#arcpy.AddMessage("layer name :" + lyr.datasetName)	
					lyrFullName = lyr.dataSource[len(os.path.realpath(key)):].replace("\COMMON",destDatabaseName)
					logging.info("modified layer name :" + lyrFullName)			       
					lyr.replaceDataSource(os.path.realpath(dictKeyValP[key]),"SDE_WORKSPACE",lyrFullName.upper(), True)
					break	

#Replaces table datasources including table full name (inputs: qualified datasource pairs, dataframe, mxd to be modified)
def ReplaceTableSource(dictKeyValP, df, inputMXD, destDatabaseName):
	for tbl in arcpy.mapping.ListTableViews(inputMXD, "", df):
		datasourceName = tbl.dataSource
		for key in dictKeyValP.keys():
			cnt = datasourceName.find(os.path.realpath(key))
			if cnt > -1:
				tblFullName = tbl.dataSource[len(os.path.realpath(key)):].replace("\COMMON",destDatabaseName)
				logging.info("modified table name :" +tblFullName)	
				tbl.replaceDataSource(os.path.realpath(dictKeyValP[key]),"SDE_WORKSPACE",tblFullName.upper(), True)
				break


main()





























































































































